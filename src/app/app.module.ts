import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddClassComponent } from './add-class/add-class.component';
import { RedirClasComponent } from './redir-clas/redir-clas.component';
import { ClasesService } from './clases.service';
import { FormsModule } from '@angular/forms';
import {HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BootPageComponent } from './boot-page/boot-page.component';
import { ViewComponent } from './view/view.component';
import { SearchClassComponent } from './search-class/search-class.component';
import { DeleteClassComponent } from './delete-class/delete-class.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { ViewStudentsComponent } from './view-students/view-students.component';
import { SearchStudentbyIdComponent } from './search-studentby-id/search-studentby-id.component';
import { DeleteStudentbyIdComponent } from './delete-studentby-id/delete-studentby-id.component';
import { RedirStudentComponent } from './redir-student/redir-student.component';
import { AllStuDetComponent } from './all-stu-det/all-stu-det.component';
import { RedirSubjectComponent } from './redir-subject/redir-subject.component';
import { AddSubjectComponent } from './add-subject/add-subject.component';
import { ViewSubjectsComponent } from './view-subjects/view-subjects.component';
import { SearchSubjectComponent } from './search-subject/search-subject.component';
import { DeleteSubjectComponent } from './delete-subject/delete-subject.component';
import { RedirTeachersComponent } from './redir-teachers/redir-teachers.component';
import { AddTeacherComponent } from './add-teacher/add-teacher.component';
import { SearchTeacherComponent } from './search-teacher/search-teacher.component';
import { DeleteTeacherComponent } from './delete-teacher/delete-teacher.component';
import { TeachersCsComponent } from './teachers-cs/teachers-cs.component';
import { ClssubComponent } from './clssub/clssub.component';

@NgModule({
  declarations: [
    AppComponent,
    AddClassComponent,
    RedirClasComponent,
    BootPageComponent,
    ViewComponent,
    SearchClassComponent,
    DeleteClassComponent,
    AddStudentComponent,
    ViewStudentsComponent,
    SearchStudentbyIdComponent,
    DeleteStudentbyIdComponent,
    RedirStudentComponent,
    AllStuDetComponent,
    RedirSubjectComponent,
    AddSubjectComponent,
    ViewSubjectsComponent,
    SearchSubjectComponent,
    DeleteSubjectComponent,
    RedirTeachersComponent,
    AddTeacherComponent,
    SearchTeacherComponent,
    DeleteTeacherComponent,
    TeachersCsComponent,
    ClssubComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ClasesService],
  bootstrap: [BootPageComponent]
})
export class AppModule { 
  

}

function goToPage(){
  console.log("Entered");
}

function teachers(){

  console.log("Entered");


}