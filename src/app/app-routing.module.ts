import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RedirClasComponent } from './redir-clas/redir-clas.component';
import { AddClassComponent } from './add-class/add-class.component';
import { ViewComponent} from './view/view.component';
import { SearchClassComponent } from './search-class/search-class.component';
import { RedirStudentComponent } from './redir-student/redir-student.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { ViewStudentsComponent } from './view-students/view-students.component';
import { SearchStudentbyIdComponent } from './search-studentby-id/search-studentby-id.component';
import { DeleteStudentbyIdComponent } from './delete-studentby-id/delete-studentby-id.component';
import { AllStuDetComponent } from './all-stu-det/all-stu-det.component';
import { AddSubjectComponent } from './add-subject/add-subject.component';
import { ViewSubjectsComponent } from './view-subjects/view-subjects.component';
import { SearchSubjectComponent } from './search-subject/search-subject.component';
import { DeleteSubjectComponent } from './delete-subject/delete-subject.component';
import { RedirSubjectComponent } from './redir-subject/redir-subject.component';
import { RedirTeachersComponent } from './redir-teachers/redir-teachers.component';
import { SearchTeacherComponent } from './search-teacher/search-teacher.component';
import { DeleteTeacherComponent } from './delete-teacher/delete-teacher.component';
import { AddTeacherComponent } from './add-teacher/add-teacher.component';
import { TeachersCsComponent } from './teachers-cs/teachers-cs.component';
import { ClssubComponent } from './clssub/clssub.component';
import { DeleteClassComponent } from './delete-class/delete-class.component';

const routes: Routes = [

  {path:"redir-class",component:RedirClasComponent},
    {path:"add-class",component:AddClassComponent},
    {path:"view",component:ViewComponent},
    {path:"search-class",component:SearchClassComponent},
    {path:"delete-class",component:DeleteClassComponent},
    {path:"redir-student",component:RedirStudentComponent},
    {path:"add-student",component:AddStudentComponent},
    {path:"view-students",component:ViewStudentsComponent},
    {path:"search-studentby-id",component:SearchStudentbyIdComponent},
    {path:"delete-studentby-id",component:DeleteStudentbyIdComponent},
    {path:"add-subject",component:AddSubjectComponent},
    {path:"view-subjects",component:ViewSubjectsComponent},
    {path:"search-subject",component:SearchSubjectComponent},
    {path:"delete-subject",component:DeleteSubjectComponent},
    {path:"all-stu-det",component:AllStuDetComponent},
    {path:"redir-subject",component:RedirSubjectComponent},
    {path:"search-teacher",component:SearchTeacherComponent},
    {path:"delete-teacher",component:DeleteTeacherComponent},
    {path:"add-teacher",component:AddTeacherComponent},
    {path:"redir-teachers",component:RedirTeachersComponent},
    {path:"teachers-cs",component:TeachersCsComponent},
    {path:"clssub",component:ClssubComponent},
    {path: '', redirectTo: 'www.google.com', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

function teachers(){

  console.log("Entered");


}