import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchStudentbyIdComponent } from './search-studentby-id.component';

describe('SearchStudentbyIdComponent', () => {
  let component: SearchStudentbyIdComponent;
  let fixture: ComponentFixture<SearchStudentbyIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchStudentbyIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchStudentbyIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
