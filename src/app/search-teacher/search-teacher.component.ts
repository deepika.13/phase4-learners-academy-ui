import { Component, OnInit } from '@angular/core';
import { Teachers } from '../teachers';
import { ClasesService } from '../clases.service';

@Component({
  selector: 'app-search-teacher',
  templateUrl: './search-teacher.component.html',
  styleUrls: ['./search-teacher.component.css']
})
export class SearchTeacherComponent implements OnInit {
  private teacherList: Teachers[];
  private All:String;

  
    constructor(private service: ClasesService) { 
      console.log(" Have entered ViewStudentsComponent component  constructor ts");  
    }
  
    viewTeachers(){
      console.log(" Have entered view class method in view component");  
  
    }
  
    ngOnInit() {
      this.All="All";
      console.log(" Have entered view component ngOnInit ts");
      this.service.viewTeachers(this.All).subscribe(data=>this.teacherList=data);
  
    }
  
  }
  