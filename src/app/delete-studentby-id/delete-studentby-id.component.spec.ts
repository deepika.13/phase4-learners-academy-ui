import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteStudentbyIdComponent } from './delete-studentby-id.component';

describe('DeleteStudentbyIdComponent', () => {
  let component: DeleteStudentbyIdComponent;
  let fixture: ComponentFixture<DeleteStudentbyIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteStudentbyIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteStudentbyIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
