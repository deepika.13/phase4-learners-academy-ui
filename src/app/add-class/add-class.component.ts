import { Component, OnInit } from '@angular/core';
import { Clasess } from '../clasess';
import { ClasesService } from '../clases.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.component.html',
  styleUrls: ['./add-class.component.css']
})
export class AddClassComponent implements OnInit {

  private clasVar: Clasess;
  private clasVarJson: String;


  constructor(private service: ClasesService, private router: Router) {
    this.clasVar = new Clasess();

  }

  createClas() {
    console.log(this.clasVar);
    console.log(JSON.stringify(this.clasVar));

    this.clasVarJson = '['.concat(JSON.stringify(this.clasVar), ']');
    console.log(this.clasVarJson);

    this.service.createClas(this.clasVarJson).subscribe(data => {this.clasVarJson;
      this.clasVar=new Clasess();
      this.router.navigate(['/view']);


    })


  }
  ngOnInit() {
    console.log("Entered add class init");

  }

}
