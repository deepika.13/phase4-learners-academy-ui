import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirSubjectComponent } from './redir-subject.component';

describe('RedirSubjectComponent', () => {
  let component: RedirSubjectComponent;
  let fixture: ComponentFixture<RedirSubjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirSubjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirSubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
