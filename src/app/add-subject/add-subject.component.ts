import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClasesService } from '../clases.service';
import { Subjects } from '../subjects';

@Component({
  selector: 'app-add-subject',
  templateUrl: './add-subject.component.html',
  styleUrls: ['./add-subject.component.css']
})
export class AddSubjectComponent implements OnInit {

  private subName:Subjects;
  private clasVarJson: String;


  constructor(private service:ClasesService,private router:Router) { 
    console.log("subjjects constr");

    this.subName = new Subjects();
   
  }

  addSubject() {
    console.log(this.subName);
    console.log(JSON.stringify(this.subName));

    this.clasVarJson = '['.concat(JSON.stringify(this.subName), ']');
    console.log(this.clasVarJson);

    this.service.addSubject(this.clasVarJson).subscribe(data => {this.clasVarJson;
     this.subName=new Subjects();
     this.router.navigate(['/view-subjects']);


    })


  }
  ngOnInit() {

    console.log("Entered add subject init");
  }

}
