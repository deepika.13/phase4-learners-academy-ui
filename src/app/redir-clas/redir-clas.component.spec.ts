import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirClasComponent } from './redir-clas.component';

describe('RedirClasComponent', () => {
  let component: RedirClasComponent;
  let fixture: ComponentFixture<RedirClasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirClasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirClasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
