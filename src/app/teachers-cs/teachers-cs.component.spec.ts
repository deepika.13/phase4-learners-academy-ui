import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachersCsComponent } from './teachers-cs.component';

describe('TeachersCsComponent', () => {
  let component: TeachersCsComponent;
  let fixture: ComponentFixture<TeachersCsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachersCsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachersCsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
