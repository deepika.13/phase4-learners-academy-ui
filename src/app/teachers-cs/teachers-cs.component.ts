import { Component, OnInit } from '@angular/core';
import { Teachers } from '../teachers';
import { ClasesService } from '../clases.service';
import { Clasess } from '../Clasess';
import { Subjects } from '../subjects';
import {Studentreport} from '../studentreport';
import {Teacherreport} from '../teacherreport';





@Component({
  selector: 'app-teachers-cs',
  templateUrl: './teachers-cs.component.html',
  styleUrls: ['./teachers-cs.component.css']
})
export class TeachersCsComponent implements OnInit {
  
  private classList: Clasess[];
  private teacherList: Teachers[];
  private subjectList: Subjects[];
  private treportList:Teacherreport[];
  private sreportList:Studentreport[];


  private All:String;
  private clasVar:Clasess;
  selectTeacherVal: any;
  selectClassVal: any;
  selectSubjectVal: any;


  constructor(private service: ClasesService) {
    this.clasVar = new Clasess();
    console.log("Entered add ");
    this.All='ALL';
    this.service.viewTeachers(this.All).subscribe(data=>this.teacherList=data);
    this.service.viewClas().subscribe(data=>this.classList=data);
    this.service.viewSubjects(this.All).subscribe(data=>this.subjectList=data);
    
  }

  // createClas() {
  //   console.log(this.clasVar);
  //   console.log(JSON.stringify(this.clasVar));

  //   this.clasVarJson = '['.concat(JSON.stringify(this.clasVar), ']');
  //   console.log(this.clasVarJson);

  //   this.service.createClas(this.clasVarJson).subscribe(data => {this.clasVarJson;
  //     this.clasVar=new Clasess();
  //     this.router.navigate(['/view']);


  //   })
 // }
  ngOnInit() {
    console.log("Entered add class init");
    //this.service.viewTeachers(this.All).subscribe(data=>this.teacherList=data);

  }
  save() {
    console.log(this.selectTeacherVal);
    console.log(this.selectClassVal);
    console.log(this.selectSubjectVal);
    this.service.assignTeachers(this.selectTeacherVal,this.selectClassVal,this.selectSubjectVal).subscribe(data=>this.treportList=data);


  }

}
