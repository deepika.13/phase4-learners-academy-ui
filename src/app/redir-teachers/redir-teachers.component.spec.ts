import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirTeachersComponent } from './redir-teachers.component';

describe('RedirTeachersComponent', () => {
  let component: RedirTeachersComponent;
  let fixture: ComponentFixture<RedirTeachersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirTeachersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirTeachersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
