import { Component, OnInit } from '@angular/core';
import { Teachers } from '../teachers';
import { ClasesService } from '../clases.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css']
})
export class AddTeacherComponent implements OnInit {

  private clasVar: Teachers;
  private clasVarJson: String;


  constructor(private service: ClasesService, private router: Router) {
    this.clasVar = new Teachers();

  }

  addTeacher() {
    console.log(this.clasVar);
    console.log(JSON.stringify(this.clasVar));

    this.clasVarJson = '['.concat(JSON.stringify(this.clasVar), ']');
    console.log(this.clasVarJson);

    this.service.addTeacher(this.clasVarJson).subscribe(data => {this.clasVarJson;
      this.clasVar=new Teachers();
      this.router.navigate(['/search-teacher']);


    })


  }
  ngOnInit() {
    console.log("Entered add class init");

  }

}
