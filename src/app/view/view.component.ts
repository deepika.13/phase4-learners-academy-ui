import { Component, OnInit } from '@angular/core';
import { Clasess } from '../clasess';
import { ClasesService } from '../clases.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
private classList: Clasess[];


  constructor(private service: ClasesService) { 
    console.log(" Have entered view component  constructor ts");  
  }

  viewClas(){
    console.log(" Have entered view class method in view component");  

  }

  ngOnInit() {
    console.log(" Have entered view component ngOnInit ts");
    this.service.viewClas().subscribe(data=>this.classList=data);

  }

}
