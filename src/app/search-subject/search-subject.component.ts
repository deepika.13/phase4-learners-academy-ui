import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ClasesService } from '../clases.service';
import { Subjects } from '../subjects';


@Component({
  selector: 'app-search-subject',
  templateUrl: './search-subject.component.html',
  styleUrls: ['./search-subject.component.css']
})
export class SearchSubjectComponent implements OnInit {

  private id1:String;

  private subjectList: Subjects[];

  
    constructor(private service: ClasesService) { 
      console.log(" Have entered ViewStudentsComponent component  constructor ts");  
    }
  
    SearchSubject(){
      console.log(" Have entered view class method in view component"+this.id1); 
      this.service.SearchSubject(this.id1).subscribe(data=>this.subjectList=data);
  
    }
  
    ngOnInit() {
      // console.log(" Have entered search student by id  component ngOnInit ts"+this.id1);
      // this.service.SearchStudentById(this.id1).subscribe(data=>this.studentList=data);
  
    }
  
  }