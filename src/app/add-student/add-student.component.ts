import { Component, OnInit } from '@angular/core';
import { Students } from '../students';
import { ClasesService } from '../clases.service';
import { Router } from '@angular/router';
import { Clasess } from '../clasess';
import { ClassField } from '@angular/compiler';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  private stud:Students;
  private  stuVarJson:String;
private stu_Class:Clasess;

  constructor(private service:ClasesService,private router:Router) { 
    this.stud = new Students();
   this.stu_Class=new Clasess();
  }

private input:string;


  addStudent(){

    console.log("inside addstudent method");
    console.log(this.stud);
    console.log(this.stud,this.stud);

//[{'stud_Name':'Deepika','stud_Badge':2,'stu_Class':{'class_Std':1}}]

   // this.stuVarJson = '['.concat(JSON.stringify(this.stud),',"stu_Class":',JSON.stringify(this.stu_Class),']');

   this.input="[{'stud_Name':'".concat(this.stud.stud_Name,"','stud_Badge':",String(this.stud.stud_Badge),",'stu_Class':{'class_Std':",String(this.stu_Class.class_Std),"}}]");

   console.log(this.input);
  //  this.stuVarJson = '['.concat(JSON.stringify(this.stud,Clasess),']');
    this.service.addStudent(this.input).subscribe(data => {this.input;
      this.stud=new Students();
      this.router.navigate(['/view-students']);
      
    })


  }
  ngOnInit() {

    console.log("entered ngon inti");

  }

}