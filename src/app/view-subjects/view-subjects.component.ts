import { Component, OnInit } from '@angular/core';
import { Subjects } from '../subjects';
import { ClasesService } from '../clases.service';

@Component({
  selector: 'app-view-subjects',
  templateUrl: './view-subjects.component.html',
  styleUrls: ['./view-subjects.component.css']
})
export class ViewSubjectsComponent implements OnInit {
  private subjectList: Subjects[];
  private All:String;
  
    constructor(private service: ClasesService) { 
      console.log(" Have entered ViewStudentsComponent component  constructor ts");  
    }
  
    viewSubjects(){
      console.log(" Have entered  viewSubjects method in view component");  
  
    }
  
    ngOnInit() {
      this.All="All";
      console.log(" Have entered view component ngOnInit ts");
      this.service.viewSubjects(this.All).subscribe(data=>this.subjectList=data);
  
    }
  
  }