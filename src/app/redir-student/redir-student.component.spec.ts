import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirStudentComponent } from './redir-student.component';

describe('RedirStudentComponent', () => {
  let component: RedirStudentComponent;
  let fixture: ComponentFixture<RedirStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
