import { Component, OnInit } from '@angular/core';
import {Studentreport} from '../studentreport';
import { ClasesService } from '../clases.service';


@Component({
  selector: 'app-all-stu-det',
  templateUrl: './all-stu-det.component.html',
  styleUrls: ['./all-stu-det.component.css']
})
export class AllStuDetComponent implements OnInit {
  private sreportList:Studentreport[];
private sreport:string;
  constructor(private service: ClasesService) { 

this.sreport = "studentreport";

  }

  ngOnInit() {
        this.service.stuReport(this.sreport).subscribe(data=>this.sreportList=data);

  }

}
