import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllStuDetComponent } from './all-stu-det.component';

describe('AllStuDetComponent', () => {
  let component: AllStuDetComponent;
  let fixture: ComponentFixture<AllStuDetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllStuDetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllStuDetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
