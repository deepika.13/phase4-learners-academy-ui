import { Component, OnInit } from '@angular/core';
import { Students } from '../students';
import { ClasesService } from '../clases.service';

@Component({
  selector: 'app-view-students',
  templateUrl: './view-students.component.html',
  styleUrls: ['./view-students.component.css']
})
export class ViewStudentsComponent implements OnInit {
  private studentList: Students[];
  
  
    constructor(private service: ClasesService) { 
      console.log(" Have entered ViewStudentsComponent component  constructor ts");  
    }
  
    viewStudents(){
      console.log(" Have entered view class method in view component");  
  
    }
  
    ngOnInit() {
      console.log(" Have entered view component ngOnInit ts");
      this.service.viewStudents().subscribe(data=>this.studentList=data);
  
    }
  
  }
  