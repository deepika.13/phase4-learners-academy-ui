import { Component, OnInit } from '@angular/core';
import { Clsreport } from '../clsreport';
import { ClasesService } from '../clases.service';
import { Clasess } from '../Clasess';
import { Subjects } from '../subjects';

@Component({
  selector: 'app-clssub',
  templateUrl: './clssub.component.html',
  styleUrls: ['./clssub.component.css']
})
export class ClssubComponent implements OnInit {

  
    private classList: Clasess[];
    private subjectList: Subjects[];
    private creportList:Clsreport[];
  
  
    private All:String;
    private clasVar:Clasess;
    selectClass: any;
    selectSubject: any;
  
  
    constructor(private service: ClasesService) {
      this.clasVar = new Clasess();
      console.log("Entered add ");
      this.All='ALL';
      this.service.viewClas().subscribe(data=>this.classList=data);
      this.service.viewSubjects(this.All).subscribe(data=>this.subjectList=data);
      
    }
  
    // createClas() {
    //   console.log(this.clasVar);
    //   console.log(JSON.stringify(this.clasVar));
  
    //   this.clasVarJson = '['.concat(JSON.stringify(this.clasVar), ']');
    //   console.log(this.clasVarJson);
  
    //   this.service.createClas(this.clasVarJson).subscribe(data => {this.clasVarJson;
    //     this.clasVar=new Clasess();
    //     this.router.navigate(['/view']);
  
  
    //   })
   // }
    ngOnInit() {
      console.log("Entered add class init");
      //this.service.viewTeachers(this.All).subscribe(data=>this.teacherList=data);
  
    }
    save() {
      console.log(this.selectClass);
      console.log(this.selectSubject);
      this.service.assignClass(this.selectClass,this.selectSubject).subscribe(data=>this.creportList=data);
  
  
    }
  
  }
  