import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClssubComponent } from './clssub.component';

describe('ClssubComponent', () => {
  let component: ClssubComponent;
  let fixture: ComponentFixture<ClssubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClssubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClssubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
