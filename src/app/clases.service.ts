import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Clasess } from './clasess';
import { Students } from './students';
import { Subjects } from './subjects';
import { Teachers } from './teachers';
import { Teacherreport } from './teacherreport';
import { Studentreport } from './studentreport';
import { Clsreport } from './clsreport';


@Injectable({
  providedIn: 'root'
})
export class ClasesService {
  private url: string;
  private id:number;
  private url1: string;
  private url2: string;
  private url3: string;
  private url4: string;
  private url5: string;

  private id1:number;


  // private viewCls: Clasess[];
  constructor(private http: HttpClient) {
    this.url = "http://localhost:8080/phase2_rest_api_proj/Classes";
    this.url1 = "http://localhost:8080/phase2_rest_api_proj/Students";
    this.url2 = "http://localhost:8080/phase2_rest_api_proj/Subjects";
    this.url3 = "http://localhost:8080/phase2_rest_api_proj/Teachers";
    this.url4 = "http://localhost:8080/phase2_rest_api_proj/TeachersDetails";
    this.url5 = "http://localhost:8080/phase2_rest_api_proj/Classes";


  }

  /*public createClas(clas:Clasess): Observable<Clasess> {

   return this.http.post<Clasess>(this.url,Clasess)

  } */


  public createClas(clas: String): Observable<String> {
    console.log(clas + 'inside createClass method of service section ');

    return this.http.post<String>(this.url, clas)

  }

  public addStudent(stu: String): Observable<String> {
    console.log(stu + 'inside addStudent method of service section ');

    return this.http.post<String>(this.url1, stu)

  }

  public viewClas(): Observable<Clasess[]> {
    console.log( 'inside viewClas method of service section');
    return this.http.get<Clasess[]>(this.url);
  }

  public viewStudents(): Observable<Students[]> {
    console.log( 'inside viewStudents method of service section');
    return this.http.get<Students[]>(this.url1);
    
  }

  public SearchClassById(id:number): Observable<Clasess> {
    console.log( 'inside viewClas method of service section');
    console.log("received number is "+id);

    return this.http.get<Clasess>(this.url+"/"+id);
  }

  public SearchStudentById(id1:number): Observable<Students[]> {
    console.log( 'inside SearchStudentById method of service section');
    console.log("received number is "+id1);

    return this.http.get<Students[]>(this.url1+"/"+id1);
  }

  public deleteClas(clas: number):Observable<Clasess[]> {

    return this.http.get<Clasess[]>(this.url);
  }

  public addSubject(sub:String): Observable<String> {
    console.log(sub + 'inside addSubject method of service section ');

    return this.http.post<String>(this.url2, sub)

  }
  public viewSubjects(All:String): Observable<Subjects[]> {
    console.log( 'inside viewStudents method of service section');
    return this.http.get<Subjects[]>(this.url2+"/"+All);
    
  }

  public SearchSubject(id:String): Observable<Subjects[]> {
    console.log( 'inside viewStudents method of service section');
    return this.http.get<Subjects[]>(this.url2+"/"+id);
    
  }

  public addTeacher(clas: String): Observable<String> {
    console.log(clas + 'inside addTeacher method of service section ');

    return this.http.post<String>(this.url3, clas)

  }

  public viewTeachers(All:String): Observable<Teachers[]> {
    console.log( 'inside viewStudents method of service section');
    return this.http.get<Teachers[]>(this.url3+"/"+All);    
  }

  public assignTeachers(tid:number,cid:number,sid:number): Observable<Teacherreport[]> {
    console.log( 'inside viewStudents method of service section');
    return this.http.get<Teacherreport[]>(this.url3+"/"+tid+"/"+cid+"/"+sid);    
  }

  public stuReport(All:String): Observable<Studentreport[]> {
    console.log( 'inside viewStudents method of service section');
    return this.http.get<Studentreport[]>(this.url4+"/"+All);    
  }

  public assignClass(cid:number,sid:number): Observable<Clsreport[]> {
    console.log( 'inside assignClasse method of service section');
    return this.http.get<Clsreport[]>(this.url5+"/"+cid+"/"+sid);    
  }

}
